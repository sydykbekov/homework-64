import React, {Component} from 'react';
import PostForm from '../../components/PostForm/PostForm';
import axios from 'axios';

class AddPost extends Component {
    state = {
        posts: [],
        newPost: {
            title: '',
            body: '',
            date: ''
        }
    };

    changeValue = event => {
        const name = event.target.name;
        const date = Date();
        this.setState({newPost: {...this.state.newPost, [name]: event.target.value, date: date}});
    };

    createPost = event => {
        event.preventDefault();

        axios.post('posts.json', this.state.newPost).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        return (
            <PostForm
                legend="Add new post"
                title={this.state.newPost.title}
                body={this.state.newPost.body}
                changeValue={this.changeValue}
                createPost={this.createPost}
            />
        )
    }
}

export default AddPost;