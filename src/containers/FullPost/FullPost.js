import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import './FullPost.css';

class FullPost extends Component {
    state = {
        singlePost: null,
        postID: ''
    };

    removePost = id => {
        axios.delete(`posts/${id}.json`).then(() => {
            this.props.history.replace('/');
        });
    };

    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`/posts/${id}.json`).then(response => {
            this.setState({singlePost: response.data, postID: id});
        });
    }

    render() {
        if (this.state.singlePost) {
            return (
                <Fragment>
                    <div className="edit-block" style={{textAlign: 'center', marginBottom: '20px'}}>
                        <Link to={`/posts/${this.state.postID}/edit`}>Edit</Link>
                        <button onClick={() => this.removePost(this.state.postID)}>Delete</button>
                    </div>

                    <div className="full-post">
                        <h2>{this.state.singlePost.title}</h2>
                        <p>{this.state.singlePost.body}</p>
                    </div>
                </Fragment>
            )
        } else {
            return <p>Loading...</p>
        }


    }
}

export default FullPost;