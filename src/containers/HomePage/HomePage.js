import React, {Component} from 'react';
import ShortPost from '../../components/ShortPost/ShortPost';
import axios from 'axios';

class HomePage extends Component {
    state = {
        posts: [],
        loading: false
    };

    componentDidMount() {
        this.setState({loading: true});

        axios.get('posts.json').then(response => {

            const posts = [];

            for (let key in response.data) {
                posts.unshift({...response.data[key], id: key});
            }
            this.setState({posts, loading: false});
        });
    }

    render() {
        if (this.state.loading) {
            return <p>Loading...</p>
        } else {
            return (
                <div className="short-posts">
                    {this.state.posts.map(post => {
                        return <ShortPost title={post.title}
                                          body={post.body}
                                          key={post.id}
                                          id={post.id}
                                          date={post.date}
                        />
                    })}
                </div>
            )
        }
    }
}

export default HomePage;