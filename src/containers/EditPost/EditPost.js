import React, {Component} from 'react';
import PostForm from '../../components/PostForm/PostForm';
import axios from 'axios';

class EditPost extends Component {
    state = {
        editPost: null,
        postID: ''
    };

    changeValue = event => {
        const name = event.target.name;
        this.setState({editPost: {...this.state.editPost, [name]: event.target.value, date: Date()}});
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`/posts/${id}.json`).then(response => {
            this.setState({editPost: response.data, postID: id});
        });
    }

    createPost = event => {
        event.preventDefault();

        axios.put(`/posts/${this.state.postID}.json`, this.state.editPost).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        if (this.state.editPost) {
            return (
                <PostForm
                    legend="Edit post"
                    title={this.state.editPost.title}
                    body={this.state.editPost.body}
                    changeValue={this.changeValue}
                    createPost={this.createPost}
                />
            )
        } else {
            return <p>Loading...</p>
        }
    }
}

export default EditPost;