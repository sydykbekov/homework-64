import React from 'react';
import {Link} from 'react-router-dom';
import './ShortPost.css';

const ShortPost = props => {
  return(
      <div className="short-post">
          <span>{props.date}</span>
          <h4>{props.title}</h4>
          <p>{props.body}</p>
          <Link to={/posts/ + props.id}>Read more</Link>
      </div>
  )
};

export default ShortPost;